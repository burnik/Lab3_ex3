#include <iostream>
#include <filesystem>
#include <fstream>
#include <string>

using namespace std;

//ios::app - Запись данных строго в конец файла, при открытии указатель в конце
//ios::ate - Запись данных в произвольном месте, при открытии указатель в конце

int main(int argc, char* argv[])
{

    #ifdef _WIN32
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    #endif
    
    //Проверяем количество аргументов
    if (argc > 2)
    {
        cerr << "Error: invalid arguments. Try this:" << endl
             << "   " << argv[0] << " file.bin" << endl;
        
        return 1;
    }
    
    //Получаем имя файла
    string fileName;
    if (argc == 1)
    {
        cout << "Введите имя файла> ";
        getline(cin, fileName);
        fileName += ".bin";
    }
    else if (argc == 2)
        fileName = argv[1];
    
    //Проверяем существует ли файл
    if (!filesystem::exists(fileName))
    {
        cout << "Файл не существует" << endl;
        return 1;
    }
    
    filesystem::path p{fileName};               //Путь к файлу
    size_t fileSize = filesystem::file_size(p); //Размер файла
    
    //Проверяем количество элементов в файле, исходя из его размера
    if (fileSize < sizeof(double) * 11)
    {
        cout << "Файл не содержит минимального (11) количества чисел. Завершение." << endl;
        return 0;
    }
    
    //Вводим число которое будет между 10 и 11
    string bufStr = "";
    cout << "Введите число которое необходимо вставить между 10 и 11>";
    getline(cin, bufStr);
    
    //Объявляем буферные переменные
    double buffer = stod(bufStr), buffer2;
    
    //Открываем файл для чтения и записи
    fstream binFile(fileName, ios::binary | ios::in | ios::out | ios::ate);
    
    //Переводим указатель на 11 элемент
    binFile.seekg(10 * sizeof(double));
    
    //Пока можем прочитать следующий элемент
    while(binFile.read((char *)&buffer2, sizeof(double)))
    {
        //Перемещаем указатель назад
        binFile.seekg(-sizeof(double), ios_base::cur);

        //Записываем данные в текущую ячейку 
        binFile.write((char *)(&buffer), sizeof(double));
        
        //Задаём элемент который будет вставлен в следующую ячейку
        buffer = buffer2;	
    }
    
    binFile.close();
    
    //Добавляем перенесённый символ в конец файла
    binFile.open(fileName, ios::binary | ios::out | ios::app);
    binFile.write((char *)(&buffer), sizeof(double));
    binFile.close();

    return 0;
}

