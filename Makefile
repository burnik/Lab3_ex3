all: fooproject binary_read

WARNINGS = -Wall
DEBUG = -ggdb -fno-omit-frame-pointer
OPTIMIZE = -O2
CPP_VERSION = c++20
CC = g++

fooproject: Makefile fooproject.cpp
	$(CC) -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) -std=$(CPP_VERSION) fooproject.cpp

binary_read: Makefile binary_read.cpp
	$(CC) -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) -std=$(CPP_VERSION) binary_read.cpp

clean:
	rm -f fooproject binary_read

# Builder will call this to install the application before running.
install:
	echo "Installing is not supported"

# Builder uses this target to run your application.
run:
	./fooproject

