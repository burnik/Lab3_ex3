# Hello, world!

## Собираем
```Bash
make Makefile
```

## Запускаем
Основная программа:
```Bash
./fooproject fileName.bin
```
Программа для чтения бинарных файлов:
```Bash
./binary_read
```
