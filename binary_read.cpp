#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <limits>

using namespace std;

void clearScreen()
{
    #ifdef WINDOWS
    //Win
    system("cls");
    #else
    
    //POSIX-like
    system("clear");
    #endif
}

void userInput(int &var)
{
    string buf;
    getline(cin, buf);
    
    var = -1;
    
    try
    {
        var = stoi(buf);
    }
    catch(invalid_argument &e)
    {
        //Неправильно введены данные
        cerr << "Error: Недопустимый ввод данных" << endl << endl;
    }
    catch(exception &e)
    {
        //Другая ошибка, которую не удалось распознать
        cerr << "Error: " << e.what() << endl << endl;
    }
}

bool createBinaryFile(string fileName, vector<double> data)
{
    try
    {
        ofstream fs(fileName, ios::out | ios::binary);
        fs.write((char *)(&data[0]), data.size() * sizeof(double));
        fs.close();
    }
    catch(exception &e)
    {
        //Другая ошибка, которую не удалось распознать
        cerr << "Error: " << e.what() << endl << endl;
        return false;
    }
    
    return true;
}

bool readBinaryFile(string fileName)
{
    //Открываем файл для чтения
    ifstream input(fileName, ios::binary);
    
    //Произошла ошибка при открытии файла, возможно, он не существует.
    if (input.fail())
        return false;
        
    size_t numbersTotal = 1;
    
    //Читаем данные из файла пока это возможно
    double buffer;
    while (input.read((char *)&buffer, sizeof(double)))
    {
        //cout.precision(numeric_limits<double>::max_digits10);
        cout << numbersTotal << ") " << buffer << endl;
        
        if (input.good())
            numbersTotal++;
        else
        {
            string bufStr = "";
            cout << "Произошла ошибка при чтении. Возможно, файл повреждён." << endl
                 << "Продолжить чтение? [y/N]>";
            getline(cin, bufStr);
            
            if (tolower(bufStr[0]) == 'y')
                continue;
                
            return false;
        }
    }
    
    //Закрываем файл
    input.close();
    
    return true;
}

int main()
{

    #ifdef _WIN32
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    #endif

    int select = 0;
    
    //Очищаем экран    
    clearScreen();
    
    do
    {
        string bufStr = "", bufStr2 = "";
        vector<double> bufVector;
        
        //Выводим меню
        cout << "Binary-read tool. Что вы хотите сделать?" << endl
             << "0) Выйти из программы" << endl
             << "1) Прочитать все данные из файла" << endl
             << "2) Записать данные в конец файла" << endl
             << "3) Создать новый файл и записать в него данные" << endl
             << "> ";
            
        //Получаем ввод пользователя 
        userInput(select);
        
        switch (select)
        {
            case 1:
                cout << "Имя файла> ";
                getline(cin, bufStr);
                
                if (readBinaryFile(bufStr + ".bin"))
                    cout << "Файл прочитан";
                else
                    cout << "Неудалось прочитать файл";
                cout << endl << endl;

                break;
            case 3:
                cout << "Имя файла> ";
                getline(cin, bufStr);
                                
                cout << "Введите данные для ввода в файл (STOP - для остановки ввода):" << endl;
                int numbersCount = 1;
                do
                {
                    cout << "(" << numbersCount << ")> ";
                    
                    try
                    {
                        getline(cin, bufStr2);
                        
                        if (bufStr2 != "STOP")
                            bufVector.push_back(stof(bufStr2));
                        
                        numbersCount++;
                        
                    }catch(exception &e)
                    {
                        cout << "Ошибка при введении данных: " << e.what() << endl;
                    }
                    
                }while(bufStr2 != "STOP");
                
                cout << "В файл будут введены эти данные:" << endl;
                for (size_t i = 0; i < bufVector.size(); i++)
                    cout << bufVector[i] << " ";
                cout << endl << endl;
                
                if (createBinaryFile(bufStr + ".bin", bufVector))
                    cout << "Данные успешно записаны";
                else
                    cout << "Ошибка при записи данных";
                cout << endl;
                
                
            break;
        
        }
        
    }while(select);
    
    cout << "(´^ω^)ノ" << endl;
    
    return 0;
}
